/*
 * TestModelPlugin.hpp
 *
 *  Created on: Apr 24, 2013
 *      Author: andrew.somerville
 */

#ifndef TESTMODELPLUGIN_HPP_
#define TESTMODELPLUGIN_HPP_


/*
 * Copyright 2012 Open Source Robotics Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
*/



#include <string>
#include <vector>

#include <boost/thread/mutex.hpp>

#include <ros/ros.h>
#include <ros/callback_queue.h>
#include <ros/advertise_options.h>
#include <ros/subscribe_options.h>

#include <geometry_msgs/Twist.h>
#include <geometry_msgs/Pose.h>
#include <geometry_msgs/Vector3.h>
#include <geometry_msgs/Wrench.h>
#include <std_msgs/String.h>

#include <boost/thread.hpp>

#include <gazebo/math/Vector3.hh>
#include <gazebo/physics/physics.hh>
#include <gazebo/physics/PhysicsTypes.hh>
#include <gazebo/transport/TransportTypes.hh>
#include <gazebo/common/Time.hh>
#include <gazebo/common/Plugin.hh>
#include <gazebo/common/Events.hh>
#include <gazebo/common/PID.hh>
#include <gazebo/sensors/SensorManager.hh>
#include <gazebo/sensors/SensorTypes.hh>
#include <gazebo/sensors/ContactSensor.hh>
#include <gazebo/sensors/ImuSensor.hh>
#include <gazebo/sensors/Sensor.hh>

#include <osrf_msgs/JointCommands.h>
#include <atlas_msgs/ControllerStatistics.h>
#include <sensor_msgs/JointState.h>

namespace gazebo
{
class TestModelPlugin : public ModelPlugin
{
        /// \brief Constructor
    public:
        TestModelPlugin();

        virtual ~TestModelPlugin();

        void Load(physics::ModelPtr _parent, sdf::ElementPtr _sdf);

    private:
        void UpdateStates();
        void RosQueueThread();
        void DeferredLoad();

        physics::WorldPtr world;
        physics::ModelPtr model;

        event::ConnectionPtr updateConnection;

        common::Time lastControllerStatisticsTime;
        double updateRate;


        // deferred loading in case ros is blocking
        sdf::ElementPtr sdf;
        boost::thread deferredLoadThread;

        // ROS stuff
        ros::NodeHandle* rosNode;
        ros::CallbackQueue rosQueue;
        boost::thread callbackQueeuThread;
        ros::Publisher pubControllerStatistics;
        ros::Publisher pubJointStates;

        ros::Subscriber subJointCommands;
        void SetJointCommands( const osrf_msgs::JointCommands::ConstPtr &_msg);

//        std::vector<std::string> jointNames;
        physics::Joint_V joints;
    private:
        class ErrorTerms
        {
            public:
                double q_p;
                double d_q_p_dt;
                double q_i;
                double qd_p;
                friend class AtlasPlugin;
        };

    private:
        std::vector<ErrorTerms> errorTerms;

        osrf_msgs::JointCommands jointCommands;
        sensor_msgs::JointState jointStates;
        boost::mutex mutex;

        // Controls stuff
        common::Time lastControllerUpdateTime;

        // controls message age measure
        atlas_msgs::ControllerStatistics controllerStatistics;
        std::vector<double> jointCommandsAgeBuffer;
        std::vector<double> jointCommandsAgeDelta2Buffer;
        unsigned int jointCommandsAgeBufferIndex;
        double jointCommandsAgeBufferDuration;
        double jointCommandsAgeMean;
        double jointCommandsAgeVariance;
        double jointCommandsAge;
};
}

#endif /* TESTMODELPLUGIN_HPP_ */
